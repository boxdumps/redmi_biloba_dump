#!/bin/bash

cat system/system/apex/com.android.vndk.current.apex.* 2>/dev/null >> system/system/apex/com.android.vndk.current.apex
rm -f system/system/apex/com.android.vndk.current.apex.* 2>/dev/null
cat system/system/apex/com.android.art.release.apex.* 2>/dev/null >> system/system/apex/com.android.art.release.apex
rm -f system/system/apex/com.android.art.release.apex.* 2>/dev/null
cat system/system/app/MiuiVideoPlayer/MiuiVideoPlayer.apk.* 2>/dev/null >> system/system/app/MiuiVideoPlayer/MiuiVideoPlayer.apk
rm -f system/system/app/MiuiVideoPlayer/MiuiVideoPlayer.apk.* 2>/dev/null
cat system/system/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null >> system/system/priv-app/MiuiCamera/MiuiCamera.apk
rm -f system/system/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null
cat system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system/system/system_ext/priv-app/Settings/Settings.apk
rm -f system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat cust/app/customized/partner-com.daraz.android_105/partner-com.daraz.android_105.apk.* 2>/dev/null >> cust/app/customized/partner-com.daraz.android_105/partner-com.daraz.android_105.apk
rm -f cust/app/customized/partner-com.daraz.android_105/partner-com.daraz.android_105.apk.* 2>/dev/null
cat cust/app/customized/partner-com.opera.browser_38/partner-com.opera.browser_38.apk.* 2>/dev/null >> cust/app/customized/partner-com.opera.browser_38/partner-com.opera.browser_38.apk
rm -f cust/app/customized/partner-com.opera.browser_38/partner-com.opera.browser_38.apk.* 2>/dev/null
cat cust/app/customized/partner-com.ss.android.ugc.trill_101/partner-com.ss.android.ugc.trill_101.apk.* 2>/dev/null >> cust/app/customized/partner-com.ss.android.ugc.trill_101/partner-com.ss.android.ugc.trill_101.apk
rm -f cust/app/customized/partner-com.ss.android.ugc.trill_101/partner-com.ss.android.ugc.trill_101.apk.* 2>/dev/null
cat cust/app/customized/partner-video.like_111/partner-video.like_111.apk.* 2>/dev/null >> cust/app/customized/partner-video.like_111/partner-video.like_111.apk
rm -f cust/app/customized/partner-video.like_111/partner-video.like_111.apk.* 2>/dev/null
cat cust/app/customized/partner-com.zhiliaoapp.musically_100/partner-com.zhiliaoapp.musically_100.apk.* 2>/dev/null >> cust/app/customized/partner-com.zhiliaoapp.musically_100/partner-com.zhiliaoapp.musically_100.apk
rm -f cust/app/customized/partner-com.zhiliaoapp.musically_100/partner-com.zhiliaoapp.musically_100.apk.* 2>/dev/null
cat cust/app/customized/partner-com.zhiliaoapp.musically_42/partner-com.zhiliaoapp.musically_42.apk.* 2>/dev/null >> cust/app/customized/partner-com.zhiliaoapp.musically_42/partner-com.zhiliaoapp.musically_42.apk
rm -f cust/app/customized/partner-com.zhiliaoapp.musically_42/partner-com.zhiliaoapp.musically_42.apk.* 2>/dev/null
cat cust/app/customized/partner-com.ebay.mobile_49/partner-com.ebay.mobile_49.apk.* 2>/dev/null >> cust/app/customized/partner-com.ebay.mobile_49/partner-com.ebay.mobile_49.apk
rm -f cust/app/customized/partner-com.ebay.mobile_49/partner-com.ebay.mobile_49.apk.* 2>/dev/null
cat cust/app/customized/partner-com.amazon.mp3_123/partner-com.amazon.mp3_123.apk.* 2>/dev/null >> cust/app/customized/partner-com.amazon.mp3_123/partner-com.amazon.mp3_123.apk
rm -f cust/app/customized/partner-com.amazon.mp3_123/partner-com.amazon.mp3_123.apk.* 2>/dev/null
cat cust/app/customized/partner-com.alibaba.aliexpresshd_54/partner-com.alibaba.aliexpresshd_54.apk.* 2>/dev/null >> cust/app/customized/partner-com.alibaba.aliexpresshd_54/partner-com.alibaba.aliexpresshd_54.apk
rm -f cust/app/customized/partner-com.alibaba.aliexpresshd_54/partner-com.alibaba.aliexpresshd_54.apk.* 2>/dev/null
cat cust/app/customized/partner-com.kwai.kuaishou.video.live_155/partner-com.kwai.kuaishou.video.live_155.apk.* 2>/dev/null >> cust/app/customized/partner-com.kwai.kuaishou.video.live_155/partner-com.kwai.kuaishou.video.live_155.apk
rm -f cust/app/customized/partner-com.kwai.kuaishou.video.live_155/partner-com.kwai.kuaishou.video.live_155.apk.* 2>/dev/null
cat cust/app/customized/partner-com.agoda.mobile.consumer_110/partner-com.agoda.mobile.consumer_110.apk.* 2>/dev/null >> cust/app/customized/partner-com.agoda.mobile.consumer_110/partner-com.agoda.mobile.consumer_110.apk
rm -f cust/app/customized/partner-com.agoda.mobile.consumer_110/partner-com.agoda.mobile.consumer_110.apk.* 2>/dev/null
cat cust/app/customized/partner-com.kwai.video_33/partner-com.kwai.video_33.apk.* 2>/dev/null >> cust/app/customized/partner-com.kwai.video_33/partner-com.kwai.video_33.apk
rm -f cust/app/customized/partner-com.kwai.video_33/partner-com.kwai.video_33.apk.* 2>/dev/null
cat cust/app/customized/partner-com.opera.browser_36/partner-com.opera.browser_36.apk.* 2>/dev/null >> cust/app/customized/partner-com.opera.browser_36/partner-com.opera.browser_36.apk
rm -f cust/app/customized/partner-com.opera.browser_36/partner-com.opera.browser_36.apk.* 2>/dev/null
cat cust/app/customized/partner-com.kwai.bulldog_112/partner-com.kwai.bulldog_112.apk.* 2>/dev/null >> cust/app/customized/partner-com.kwai.bulldog_112/partner-com.kwai.bulldog_112.apk
rm -f cust/app/customized/partner-com.kwai.bulldog_112/partner-com.kwai.bulldog_112.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/Gmail2/Gmail2.apk.* 2>/dev/null >> product/app/Gmail2/Gmail2.apk
rm -f product/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat product/app/Messages/Messages.apk.* 2>/dev/null >> product/app/Messages/Messages.apk
rm -f product/app/Messages/Messages.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat product/app/YouTube/YouTube.apk.* 2>/dev/null >> product/app/YouTube/YouTube.apk
rm -f product/app/YouTube/YouTube.apk.* 2>/dev/null
cat product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> product/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat product/app/Maps/Maps.apk.* 2>/dev/null >> product/app/Maps/Maps.apk
rm -f product/app/Maps/Maps.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> product/priv-app/Velvet/Velvet.apk
rm -f product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat product/data-app/Photos/Photos.apk.* 2>/dev/null >> product/data-app/Photos/Photos.apk
rm -f product/data-app/Photos/Photos.apk.* 2>/dev/null
